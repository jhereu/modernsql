package com.buntplanet.formacion.modernsql;

import java.sql.Connection;

class BatchInserts {
  public static void main(String[] args) throws Throwable {
    Connection conn = Setup.getSingleConnection();
    Setup.prepareDBNoIndexes(conn);
    InsertModes.batchInserts(10_000, conn);
    conn.close();
  }
}
